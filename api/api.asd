;;;; api.asd
;;;; This file has the definition of the lisp systems in it
;;;; Systems are somewhat equivalent to libraries
;;;; This is the de-facto standard for CL these days

(asdf:defsystem "matzy-api"
  :description "Test matzy api for hunchentoot"
  :author "CHris Matzenbach <matzy@protonmail.com>"
  :license  "GNU"
  :version "0.0.1"
  :depends-on ("huchentoot" "easy-routes" "cl-who")
  ;; the files that are involved in our system
  :components ((:file "server")
               (:file "main" :depends-on ("server"))))

