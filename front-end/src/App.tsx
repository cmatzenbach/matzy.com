import React from 'react';
/* import { BrowserRouter as Router, Route, Switch, useLocation } from 'react-router-dom'; */
import {BrowserRouter as Router} from 'react-router-dom';
import {UserContext} from './contexts/UserContext';
// components
import TopNav from './components/TopNav';
import Login from './components/Login';

const App: React.FC = () => {
  return (
    <UserContext.Provider value="chickens">
      <Router />
        <div className="app">
          <TopNav />
          <Login />
        </div>
    </UserContext.Provider>
  );
}

export default App;
