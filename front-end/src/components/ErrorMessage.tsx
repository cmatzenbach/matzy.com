import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  ErrorMessage: {
    color: '#ff0000',
    marginTop: '10px',
    textAlign: 'center',
  },
});

const classes = useStyles();

const ErrorMessageContainer: React.FC<{ errorMessage: string | null }> = ({
  errorMessage
}) => {
  return <p className={classes.ErrorMessage}>{errorMessage}</p>;
};

export default ErrorMessageContainer;
