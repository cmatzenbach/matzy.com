import React from 'react';
import useAxios from 'axios-hooks'
import Button from '@material-ui/core/Button';
import './App.css';

const testGet: React.FC<any> = () => {
	const [{ data, loading, error }, refetch] = useAxios(
		'http://localhost:4242/testjson'
	);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;

  return (
		<div className="App">
			<Button variant="contained" color="primary" onClick={() => refetch()}>Refetch</Button>
			<pre>{JSON.stringify(data, null, 2)}</pre>
		</div>
	);
}

export default testGet;
